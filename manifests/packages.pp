# == Class: builder::packages
#
# Installs / updates all the common packages used on Emerging Technology build systems
#
# automatically included by builder module
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016 The Board of Trustees of the Leland Stanford Junior
# University
#
class builder::packages (
  $install,
  $uninstall,
  $package_type,
){

  include stdlib

  Package { provider => $package_type }

  class { "${module_name}::${package_type}": }

  $only_install = difference($install, $uninstall)

  ensure_packages($only_install, {'ensure' => 'latest'})
  ensure_packages($uninstall,    {'ensure' => 'absent'})


}
