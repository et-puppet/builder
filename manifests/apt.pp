# == Class: builder::apt
#
# APT configuration for build systems
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016-2017 The Board of Trustees of the Leland Stanford
# Junior University
#
class builder::apt (
  $backports = undef,
  $debs3_version = 'latest',
  $mini_portile_version = undef,
) {

  include apt;

  # Sometimes we need the backport version of packages
  if $backports {
    apt::pin { 'backports':
      packages => $backports,
      priority => 500,
      codename => "${::lsbdistcodename}-backports",
      notify   => Exec['apt_update'],
    }
  }

  # force apt-get update before package installation
  Class['apt::update'] -> Package<| |>

  # Debian.yaml should set debs3_version to latest
  # for all Debian-based distros
  # Distro-specific YAML files can override the version
  # Trusty and Wheezy also require an older version
  # of mini_portile
  if $mini_portile_version {
    package { 'mini_portile':
      ensure   => $mini_portile_version,
      provider => 'gem',
      before   => Package['deb-s3'],
    }
  }

  package { 'ruby-nokogiri':
    ensure  => latest,
    before  => Package['deb-s3'],
    require => Package['ruby-dev','rubygems-integration'],
  }

  package { 'deb-s3':
    ensure   => $debs3_version,
    provider => 'gem',
  }

}
