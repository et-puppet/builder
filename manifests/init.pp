# == Class: builder
#
# Configuration for Emerging Technology build systems.
#
# === Parameters
#
# username: defaults to jenkins
# ssh_key_type: defaults to ssh-rsa
# ssh_key: defaults to the Jenkins deploy key
#
# === Examples
#
#  include builder
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016 The Board of Trustees of the Leland Stanford Junior
# University
#
class builder (
  $username,
  $ssh_key_type,
  $ssh_key
) {

  include builder::packages

  file { '/var/run/sshd':
    ensure => directory,
    mode   => '0755',
    owner  => 'root'
  }

  user { $username:
    ensure         => present,
    name           => $username,
    managehome     => true,
    home           => "/home/${username}",
    comment        => $username,
    shell          => '/bin/bash',
    purge_ssh_keys => true,
    groups         => [ 'sudo' ],
  }

  file { "/home/${username}/.ssh":
    ensure  => directory,
    owner   => $username,
    mode    => '0700',
    require => User[$username],
  }

  ssh_authorized_key { $username:
    user    => $username,
    type    => $ssh_key_type,
    key     => $ssh_key,
    require => File["/home/${username}/.ssh"],
  }

  file { "/home/${username}/.gbp.conf":
    ensure  => file,
    source  => "puppet:///modules/${module_name}/gbp.conf",
    owner   => $username,
    mode    => '0644',
    require => User[$username],
  }

  file { "/etc/sudoers.d/${username}":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0440',
    content => "${username} ALL=(ALL) NOPASSWD:ALL",
    require => User[$username],
  }

}
